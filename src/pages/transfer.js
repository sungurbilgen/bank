import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import t from 'prop-types';
import {
  H1,
  Form,
  FormGroup,
  Input,
  Col,
  Button,
  Option,
  Row,
} from '@bootstrap-styled/v4';
import {
  setTransfer,
  replaceAccounts,
  resetTransfer,
} from '../actions/creator';
import { transfer, getAccounts } from '../actions/api';
import { history } from '../utils';

const TransferForm = ({
  accounts,
  fromAccount,
  toAccount,
  amount,
  changeFromAccount,
  changeToAccount,
  changeAmount,
  submit,
}) => (
  <Form onSubmit={submit}>
    <FormGroup>
      <Input
        className="form-control"
        type="select"
        placeholder="From"
        onChange={changeFromAccount}
        value={fromAccount}
      >
        <Option>Pick one</Option>
        {accounts.map((x) => (
          <Option value={x.accountId}>{x.name}</Option>
        ))}
      </Input>
    </FormGroup>
    <FormGroup>
      <Input
        className="form-control"
        type="select"
        placeholder="To"
        onChange={changeToAccount}
        value={toAccount}
      >
        <Option>Pick one</Option>
        {accounts.map((x) => (
          <Option value={x.accountId}>{x.name}</Option>
        ))}
      </Input>
    </FormGroup>
    <FormGroup>
      <Input
        type="text"
        className="form-control"
        placeholder="Amount"
        value={amount}
        onChange={changeAmount}
      />
    </FormGroup>

    <Button color="primary" type="submit">
      {'Transfer'}
    </Button>
  </Form>
);
TransferForm.propTypes = {
  accounts: t.array.isRequired,
  fromAccount: t.string.isRequired,
  toAccount: t.string.isRequired,
  amount: t.string.isRequired,
  changeFromAccount: t.func.isRequired,
  changeToAccount: t.func.isRequired,
  changeAmount: t.func.isRequired,
  submit: t.func.isRequired,
};

const TransferFormContainer = () => {
  const accessToken = useSelector((state) => state.auth.accessToken);
  const customerId = useSelector((state) => state.loginForm.customerId);
  const accounts = useSelector((state) => state.accounts.allAccounts);
  const fromAccount = useSelector((state) => state.transferForm.from);
  const toAccount = useSelector((state) => state.transferForm.to);
  const amount = useSelector((state) => state.transferForm.amount);

  const dispatch = useDispatch();
  const changeFromAccount = useCallback(
    (e) => dispatch(setTransfer('from', e.target.value)),
    [dispatch],
  );
  const changeToAccount = useCallback(
    (e) => dispatch(setTransfer('to', e.target.value)),
    [dispatch],
  );
  const changeAmount = useCallback(
    (e) => dispatch(setTransfer('amount', e.target.value)),
    [dispatch],
  );

  const submit = useCallback(
    (e) => {
      e.preventDefault();
      transfer(customerId, accessToken, fromAccount, toAccount, amount).then(
        () => {
          dispatch(resetTransfer());
          getAccounts(customerId, accessToken).then((y) => {
            dispatch(replaceAccounts(y.data.items));
            history.push('/welcome');
          });
        },
      );
    },
    [dispatch, accessToken, customerId, fromAccount, toAccount, amount],
  );
  return (
    <TransferForm
      accounts={accounts}
      fromAccount={fromAccount}
      toAccount={toAccount}
      amount={amount}
      changeFromAccount={changeFromAccount}
      changeToAccount={changeToAccount}
      changeAmount={changeAmount}
      submit={submit}
    />
  );
};

const Transfer = () => (
  <>
    <H1>Transfer</H1>

    <Row>
      <Col xs="10" md="4">
        <TransferFormContainer />
      </Col>
    </Row>
  </>
);

export default Transfer;
