import { history } from '../utils';

const Logout = () => {
  window.localStorage.removeItem('loginForm');
  history.push('/login');
  window.location.reload();
  return null;
};

export default Logout;
