import React from 'react';
import t from 'prop-types';
import { useSelector } from 'react-redux';
import { H1, H2, Row, Col } from '@bootstrap-styled/v4';

const Account = ({ name, accountNumber, available, balance }) => (
  <Row>
    <Col>
      <H2>{name}</H2>
      <strong>{accountNumber}</strong>
      <br />
      <strong>Available:</strong>
      {available}
      <br />
      <strong>Balance:</strong>
      {balance}
      <br />
      <br />
    </Col>
  </Row>
);

Account.propTypes = {
  name: t.string.isRequired,
  accountNumber: t.string.isRequired,
  available: t.string.isRequired,
  balance: t.string.isRequired,
};

const Home = ({ accounts }) => (
  <>
    <H1>Home</H1>

    {accounts.map((x) => (
      <Account
        key={x.accountId}
        name={x.name}
        accountNumber={x.accountNumber}
        available={x.available}
        balance={x.balance}
      />
    ))}
  </>
);

Home.propTypes = {
  accounts: t.array.isRequired,
};
const HomeContainer = () => {
  const accounts = useSelector((x) => x.accounts.allAccounts);
  return <Home accounts={accounts} />;
};
export default HomeContainer;
