import React from 'react';
import { Nav } from '@bootstrap-styled/v4';
import Container from './container';
import MenuItem from './menuItem';

const PublicMenu = () => (
  <Container>
    <Nav navbar className="mr-auto">
      <MenuItem title="Home" url="/" />
      <MenuItem title="Login" url="/login" />
      <MenuItem title="Transfer" url="/transfer" />
      <MenuItem title="Logout" url="/logout" />
    </Nav>
  </Container>
);

export default PublicMenu;
