import React from 'react';
import t from 'prop-types';
import { NavItem, NavLink } from '@bootstrap-styled/v4';
import { history } from '../../utils';

const MenuItem = ({ title, url }) => (
  <NavItem>
    <NavLink onClick={() => history.push(url)}>{title}</NavLink>
  </NavItem>
);

MenuItem.propTypes = {
  title: t.string.isRequired,
  url: t.string.isRequired,
};

export default MenuItem;
